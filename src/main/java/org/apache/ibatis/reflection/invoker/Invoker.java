/**
 * Copyright ${license.git.copyrightYears} the original author or authors.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.ibatis.reflection.invoker;

import java.lang.reflect.InvocationTargetException;

/**
 * 关于对象类中的属性值获取和设置可以分为 Field 字段的 get/set 还有普通的 Method 的调用，
 * 为了减少使用方的过多的处理，这里可以把集中调用者的实现包装成调用策略，统一接口不同策略不同的实现类
 * @author Clinton Begin
 */
public interface Invoker {

  /**
   * 方法执行调用器，该方法负责完成对象方法的调用和对象属性的读写
   *
   * @param target
   * @param args
   * @return
   * @throws IllegalAccessException
   * @throws InvocationTargetException
   */
  Object invoke(Object target, Object[] args) throws IllegalAccessException, InvocationTargetException;

  /**
   * 传入参数或者传出参数的类型（如有一个入参就是入参类型，否则是出参类型）
   *
   * @return
   */
  Class<?> getType();
}
